# Data Loader (NodeJs CLI)

This repo contains the code for the Data Loader Command Line Application written in Javascript for NodeJs runtime environment.
The application streams CSV from a URL, process and write data from each row to a specified database table.

## Tech Stack

- Programming Language: Javascript (NodeJS)
- Database: PostgreSQL
- ORM: Sequelize

## Instruction on running the app on your local machine

NB: Before running the app locally, edit the content of the [.env](./.env) file to match your DB connection details as well as the link to the CSV URL.

After cloning this repo to your local machine and while in the project's root directory in Terminal or Windows CMD, you should run:

### `npm install`

This will install the required dependencies for the app.

## To start the app

run:

### `npm start`

This command will run the app in your terminal or command line window

## To start Jest Unit Testing:

run:

### `npm run test`
