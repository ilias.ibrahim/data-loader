const config = require("../src/config/config");
const { isValidCsvUrl } = require("../src/utils/utils");

describe("Csv URL Validation", () => {
  test("it should return false for invalid csv url", async () => {
    const url = "https://domain.com/file";
    expect(await isValidCsvUrl(url)).toBeFalsy();
  });

  test("it should return true for valid csv url", async () => {
    const url = "https://raw.githubusercontent.com/iliasi/data/main/orders.csv";
    expect(await isValidCsvUrl(url)).toBeTruthy();
  });
});
