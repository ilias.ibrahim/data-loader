const fetch = require("node-fetch");

const isValidCsvUrl = async (url) => {
  const response = await fetch(url);
  return response.status === 200 && url.endsWith(".csv");
};

module.exports = { isValidCsvUrl };
