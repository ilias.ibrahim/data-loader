const { DataTypes } = require("sequelize");
const { sequelize } = require("../db");

const Order = sequelize.define(
  "orders",
  {
    orderId: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true,
    },

    customerId: {
      type: DataTypes.STRING,
    },
    item: {
      type: DataTypes.STRING,
    },

    quantity: {
      type: DataTypes.INTEGER,
    },
  },
  { timestamps: false }
);

Order.sync().then(() => {
  console.log("Order Model synced");
});

module.exports = Order;
