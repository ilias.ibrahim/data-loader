const { DataTypes } = require("sequelize");
const { sequelize } = require("../db");

const Customer = sequelize.define(
  "customers",
  {
    customerId: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true,
    },

    firstName: {
      type: DataTypes.STRING,
    },
    lastName: {
      type: DataTypes.INTEGER,
    },
  },
  { timestamps: false }
);

Customer.sync().then(() => {
  console.log("Customer Model synced");
});

module.exports = Customer;
