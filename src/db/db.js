const config = require("../config/config");
const { Sequelize } = require("sequelize");

const sequelize = new Sequelize(config.dbName, config.dbUser, config.dbPass, {
  host: config.dbHost,
  port: config.dbPort,
  dialect: "postgres",
  logging: false,
  pool: {
    max: 200,
    min: 0,
    acquire: 60000,
    idle: 10000,
  },
});

const checkDatabaseConnection = async () => {
  try {
    await sequelize.authenticate();
    console.log("Database connection established!");
  } catch (error) {
    console.error("Unable to connect to the database:");
  }
};

module.exports = {
  sequelize,
  checkDatabaseConnection,
};
