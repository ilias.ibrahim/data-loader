const {
  checkDatabaseConnection,
  closeDatabaseConnection,
  sequelize,
} = require("./db/db");
const csv = require("csvtojson");
const request = require("request");

const Order = require("./db/models/orders");
const Customer = require("./db/models/customers");
const { csvUrl } = require("./config/config");
const { isValidCsvUrl } = require("./utils/utils");

async function main() {
  const isValidCsv = await isValidCsvUrl(csvUrl);
  if (!isValidCsv) {
    console.log("Invalid CSV url");
    process.exit();
  }

  await checkDatabaseConnection();

  console.time("Processed in");
  csv()
    .fromStream(request.get(csvUrl))
    .on("header", () => {
      console.log("Loading Orders From CSV....");
    })
    .subscribe(
      (row) => {
        return new Promise(async (resolve) => {
          const customerExists = await Customer.findOne({
            where: {
              customerId: row.customerId,
            },
          });
          if (customerExists) {
            Order.create({
              orderId: row.orderId,
              customerId: row.customerId,
              item: row.item,
              quantity: Number(row.quantity),
            });
          }
          resolve();
        });
      },
      (onError) => console.log("Error while loading CSV:", onError),
      (onComplete) => {
        console.log("CSV successfully loaded to database", onComplete);
        console.timeEnd("Processed in");
      }
    );
}

main();
